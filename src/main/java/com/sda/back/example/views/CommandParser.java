package com.sda.back.example.views;

import com.sda.back.example.controller.UserController;

public class CommandParser {
    private UserController userController = new UserController();
    private SessionCookies cookies = new SessionCookies();

    public void parseLine(String line) {
        String[] words = line.trim().split(" ");
        if (words[0].toLowerCase().equals("user")) {
            parseUserCommand(words);
        }
    }

    private void parseUserCommand(String[] words) {
        if (words[1].equals("register")) {
            userController.registerUser(words[2], words[3]);
        } else if (words[1].equalsIgnoreCase("login")) {
            // jeśli nie jesteśmy zalogowani
            if (!cookies.isLoggedIn()) {
                // próbujemy sie zalogować
                if (userController.login(words[2], words[3])) {
                    // zalogowani
                    cookies.setLoggedIn(true);
                } else {
                    // nieudana próba logowania
                    cookies.setLoggedIn(false);
                }
            }
        }
    }
}
