package com.sda.back.example.views;

public class SessionCookies {
    private boolean isLoggedIn = false;
    private String sessionId = null;

    public SessionCookies() {
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
