package com.sda.back.example.service;

public class Services {
    private static final Services INSTANCE = new Services();

    private final AuctionService auctionService = new AuctionServiceImpl();
    private final UserService userService = new UserServiceImpl();

    private Services() {
    }

    public static Services getInstance() {
        return INSTANCE;
    }

    public AuctionService getAuctionService() {
        return auctionService;
    }

    public UserService getUserService() {
        return userService;
    }
}
