package com.sda.back.example.service;

import com.sda.back.example.model.User;

public interface UserService {
    User getUserWithId(long id);

    boolean registerUser(String login, String password);

    boolean userExists(String login);

    Long login(String login, String pass);
}
