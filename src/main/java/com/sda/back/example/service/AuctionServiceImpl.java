package com.sda.back.example.service;

import com.sda.back.example.model.Auction;

import java.util.HashMap;

public class AuctionServiceImpl implements AuctionService {
    private static int AUCTION_COUNTER = 0;

    private HashMap<Long, Auction> auctions = new HashMap<>();

    protected AuctionServiceImpl() {
    }

    @Override
    public boolean addAuction(String title, String desc, double price, int quantity, long ownerId) {
        if (ownerId <= 0) {
            return false;
        }

        Auction newAuction = new Auction(AUCTION_COUNTER++, ownerId, title, desc, price, quantity);
        auctions.put(newAuction.getId(), newAuction);

        return true;
    }

}
