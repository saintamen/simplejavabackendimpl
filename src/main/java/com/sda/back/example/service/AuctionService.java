package com.sda.back.example.service;

import com.sda.back.example.model.Auction;

public interface AuctionService {
    boolean addAuction(String title, String desc, double price, int quantity, long ownerId);
}
