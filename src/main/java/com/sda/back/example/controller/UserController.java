package com.sda.back.example.controller;

import com.sda.back.example.service.Services;
import com.sda.back.example.service.UserService;
import com.sda.back.example.service.UserServiceImpl;

import java.util.Optional;

public class UserController {

    private UserService userService = Services.getInstance().getUserService();

    // Odpowiada za odpowiednie wywołanie zadania na serwisie, oraz wyświetlenie odpowiedzi

    public void registerUser(String login, String password) {
        if (userService.registerUser(login, password)) {
            System.out.println("Udało Ci się zarejestrować, Witamy!");
        } else {
            System.err.println("Nie udało się zarejestrować :(");
        }
    }

    /**
     * Loguje użytkownika.
     * @param login - login użytkownika.
     * @param pass - hasło użytkownika
     * @return Zwraca Optional z id użytkownika jeśli uda mu się zalogować.
     */
    public Optional<Long> login(String login, String pass) {
        return Optional.ofNullable(userService.login(login, pass));
    }
}
